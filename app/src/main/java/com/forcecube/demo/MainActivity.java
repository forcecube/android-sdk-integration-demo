package com.forcecube.demo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.forcecube.sdk.ForceCuBe;


/**
 * Created by aaistom on 4/16/16.
 */
public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 2;

    private TextView mSdkLogTextView;
    private Button mGettingOffersButton;

    private ForceCuBe.Manager mForceCubeManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mSdkLogTextView = (TextView) findViewById(R.id.main_activity_tv_sdk_log);
        mGettingOffersButton = (Button) findViewById(R.id.main_activity_btn_accepted_offers);
        mGettingOffersButton.setOnClickListener(mGettingOffersClickListener);
        mGettingOffersButton.setEnabled(false);

        logOnDisplay("ForceCube start with: " + MainApp.APP_DEV_KEY + " and " + MainApp.APP_DEV_SECRET);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        requestPermissions();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        logOnDisplay("bind ForceCuBeManager");
        mForceCubeManager = ForceCuBe.bind(this, mForceCubeListener);
    }

    @Override
    protected void onPause() {

        if (mForceCubeManager != null) {
            logOnDisplay("unbind ForceCuBeManager");
            mForceCubeManager.unbind();

            mForceCubeManager = null;
        }

        super.onPause();
    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    ForceCuBe.startWithPermissions(this);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    ForceCuBe.startWithPermissions(this);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    private void logOnDisplay(String str) {
        mSdkLogTextView.append(str);
        mSdkLogTextView.append("\n");

        Log.v(TAG, str);
    }

    private final View.OnClickListener mGettingOffersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//            logOnDisplay("Request on unopened offers");
//            mForceCubeManager.getUnopenedOffers(mSdkUnopenedOffersCallback);
//            mGettingOffersButton.setEnabled(false);
        }
    };

//    private final SdkCallback<List<CampaignOffer>> mSdkUnopenedOffersCallback = new SdkCallback<List<CampaignOffer>>() {
//        @Override
//        public void onSuccess(List<CampaignOffer> campaignOffers) {
//            logOnDisplay("" + campaignOffers.size() + " unopened offers received");
//
//            if (!campaignOffers.isEmpty()) {
//
//                String[] dialogItems = new String[campaignOffers.size()];
//                for (int i = 0; i < dialogItems.length; ++i)
//                    dialogItems[i] = String.format("%d) %s", campaignOffers.get(i).getId(), campaignOffers.get(i).getFullScreenTitle());
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                builder.setTitle("Unopened offers");
//                builder.setItems(dialogItems, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        // empty
//                    }
//                });
//
//                AlertDialog alert = builder.create();
//                alert.show();
//            }
//
//            mGettingOffersButton.setEnabled(true);
//        }
//
//        @Override
//        public void onError(Throwable throwable) {
//            logOnDisplay("Error in getting unopened offers list: " + throwable.getMessage());
//            mGettingOffersButton.setEnabled(true);
//        }
//    };

    private final ForceCuBe.SdkListener mForceCubeListener = new ForceCuBe.SdkListener() {
        @Override
        public void onSdkStatus(ForceCuBe.State state, ForceCuBe.FatalError error) {
            logOnDisplay("onSdkStatus: " + state.name() + " " + error.name());

            if (((state == ForceCuBe.State.STARTED) || (state == ForceCuBe.State.STARTED_WITH_GEOFENCING)) &&
                    (error == ForceCuBe.FatalError.NO_ERROR)) {

                mGettingOffersButton.setEnabled(true);
            }
        }

        @Override
        public void onSdkRuntimeError(ForceCuBe.RuntimeError runtimeError) {
            logOnDisplay("onSdkRuntimeError: " + runtimeError.name());
        }

//        @Override
//        public void onCampaignOfferDelivered(long l) {
//
//        }
    };
}
